//
//  UIView + ext.swift
//  WorkoutApp
//
//  Created by Perizat Omar on 13.05.2023.
//

import UIKit

extension UIView {
    func addButtonBorder(with color: UIColor, height: CGFloat) {
        let separator = UIView()
        separator.backgroundColor = color
        separator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        separator.frame = CGRect(x: 0,
                                 y: frame.height - height,
                                 width: frame.width,
                                 height: height)
        addSubview(separator)
    }
}
