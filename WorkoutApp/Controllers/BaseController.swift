//
//  BaseController.swift
//  WorkoutApp
//
//  Created by Perizat Omar on 17.04.2023.
//

import UIKit

class BaseController: UIViewController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
}

@objc extension BaseController {
    
    func addViews() {}
    
    func layoutViews() {}
    
    // MARK: - Setup View
    
    func setupView() {
        view.backgroundColor = Resources.Colors.background
    }
}
