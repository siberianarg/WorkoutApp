//
//  NavBarController.swift
//  WorkoutApp
//
//  Created by Perizat Omar on 06.04.2023.

import UIKit

final class NavBarController: UINavigationController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: - Setup View
    
    private func setupView() {
        view.backgroundColor = .white
        navigationBar.isTranslucent = false
        navigationBar.standardAppearance.titleTextAttributes = [
            .foregroundColor: Resources.Colors.titleGray,
            .font: Resources.Fonts.helvelticaRegular(with: 17)
        ]
        
        navigationBar.addButtonBorder(with: Resources.Colors.separator, height: 1)
    }
}



